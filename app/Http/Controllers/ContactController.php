<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $contacts = Contact::all();

        return Inertia::render(
            'Contacts/Index',
            [
                'contacts' => $contacts
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        return Inertia::render(
           'Contacts/Create'
       );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email:rfc,dns',
            'phone' => 'required|regex:/[0-9]/',
        ]);
        Contact::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone
        ]);
       

        return redirect()->route('contacts.index')->with('message', 'Contact Created Successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(Contact $contact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Contact $contact)
    {
        //
        return Inertia::render(
           'Contacts/Edit',
           [
               'contact' => $contact
           ]
       );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Contact $contact)
    {
      
       $request->validate([
        'name' => 'required|string|max:255',
        'email' => 'required|email:rfc,dns',
        'phone' => 'required|regex:/[0-9]/',
    ]);


       $contact->name = $request->name;
       $contact->email = $request->email;
       $contact->phone = $request->phone;
       $contact->save();


       return redirect()->route('contacts.index')->with('message', 'Contact Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Contact $contact)
    {
        //
        $contact->delete();
   

      return redirect()->route('contacts.index')->with('message', 'Contact Delete Successfully');
    }
}
